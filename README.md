# Epik Elixir

Client library for the [Epik.com v2 API](https://docs.userapi.epik.com/v2/).

## Installation

The package can be installed by adding `epik` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:epik, "~> 0.2.4"}
  ]
end
```

### Configuration

You will then need to configure Epik for your application.

```elixir
import Config

config :epik,
  signature: "****-****-****-****",
  endpoint: "https://usersapiv2.epik.com"
```

#### Supported options

- `signature` (**required**): Found in the API Settings of your Epik account.
- `endpoint` (**required**): The endpoint to use for requests. Set this to Epik's API endpoint in production. There is no default for safety reasons.
- `http_client` (optional): An `HTTPoison` compatible module. Used internally by `Epik.Client` to make requests. Defaults to `HTTPoison`.
- `json_parser` (optional): A `Jason` compatible parser module. Parses the response body in `Epik.Client`. Defaults to `Jason`.
- `timeout` (optional): Timeout for HTTP responses. Defaults to `60_000`.

## Usage

Use the `Epik` module to execute simple commands.
Everything else can be done by calling `Epik.Client` directly.

### Checking domains

To check the price and availability of domains, use `Epik.check_domains/1`.

```elixir
iex(1)> Epik.check_domains(["tribes.host", "fediverse.gold"])
{:ok,
 %HTTPoison.Response{
   status_code: 200,
   body: %{
     "code" => 1000,
     "data" => %{
       "FEDIVERSE.GOLD" => %{
         "available" => 1,
         "domain" => "FEDIVERSE.GOLD",
         "premium" => 0,
         "price" => 5.49,
         "supported" => 1
       },
       "TRIBES.HOST" => %{
         "available" => 0,
         "available_reason" => "in use",
         "domain" => "TRIBES.HOST",
         "premium" => 0,
         "supported" => 1
       }
     },
     "message" => "Command completed successfully."
   }
 }}
```

### Registering a domain

To register a domain, call `Epik.register_domain/2`.

```elixir
iex(1)> Epik.register_domain("fedigold.xyz", 1)
{:ok,
 %HTTPoison.Response{
   status_code: 200,
   body: %{
     "code" => 1000,
     "data" => %{
       "FEDIGOLD.XYZ" => %{
         "error" => 0,
         "message" => "Successfully created",
         "payment" => %{
           "error" => 0,
           "paymentStatus" => true,
           "paymentValue" => 0.99,
           "period" => "1",
           "pricePerPeriod" => 0.99
         },
         "paymentStatus" => true,
         "paymentValue" => 0.99,
         "period" => "1",
         "pricePerPeriod" => 0.99
       }
     },
     "message" => "Command completed successfully.",
     "period" => "1",
     "total" => %{
       "amount" => 0.99,
       "message" => "Withdraw money successfully",
       "method" => "Balance",
       "success" => true
     }
   }
 }}
```

### Other actions

All other actions may be performed by using `Epik.Client` directly.
For example:

```elixir
iex(1)> Epik.Client.get("/v2/users", %{"EMAIL" => "herman@munster.me"})
{:ok,
 %HTTPoison.Response{
   status_code: 200
   body: %{
     "code" => 1000,
     "message" => "Command completed successfully.",
     "user" => %{
       "address1" => "1313 Mockingbird Lane",
       "city" => "Mockingbird Heights",
       "country" => "US",
       "email" => "herman@munster.me",
       "firstName" => "Herman",
       "id" => "1313131",
       "lastName" => "Munster",
       "organization" => "",
       "phone" => "+1.1234567890",
       "state" => "NY",
       "username" => "munster",
       "zip" => "12200"
     }
   }
 }}
```

See the [Epik.com v2 API docs](https://docs.userapi.epik.com/v2/) for a full list of functions.

## License

Epik Elixir is licensed under the MIT license.
See `LICENSE.md` for a full copy of the license.