import Config

config :epik,
  http_client: Epik.HTTPoisonMock,
  signature: "0000-0000-0000-0000",
  endpoint: "https://api.epik.test"
