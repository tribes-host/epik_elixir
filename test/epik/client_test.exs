defmodule Epik.ClientTest do
  use ExUnit.Case

  test "get/4 returns a valid response" do
    expected = %HTTPoison.Response{
      status_code: 200,
      body: %{
        "code" => 1000,
        "data" => %{
          "FEDIVERSE.GOLD" => %{
            "available" => 1,
            "domain" => "FEDIVERSE.GOLD",
            "premium" => 0,
            "price" => 5.49,
            "supported" => 1
          },
          "TRIBES.HOST" => %{
            "available" => 0,
            "available_reason" => "in use",
            "domain" => "TRIBES.HOST",
            "premium" => 0,
            "supported" => 1
          }
        },
        "message" => "Command completed successfully."
      }
    }

    params = %{"DOMAINS" => "tribes.host,fediverse.gold"}
    assert {:ok, ^expected} = Epik.Client.get("/v2/domains/check", params)
  end

  test "get/4 returns with an error" do
    expected = %HTTPoison.Response{
      status_code: 400,
      body: %{
        "errors" => [
          %{
            "code" => 2,
            "description" => "Wrong param This tld (hodl) is not supported",
            "message" => "Invalid parameter"
          }
        ]
      }
    }

    params = %{"DOMAINS" => "tribes.hodl"}
    assert {:ok, ^expected} = Epik.Client.get("/v2/domains/check", params)
  end
end
