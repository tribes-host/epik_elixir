defmodule Epik.HTTPoisonMock do
  def request(%HTTPoison.Request{
        url: "https://api.epik.test/v2/domains/check",
        params: %{
          "DOMAINS" => "tribes.host,fediverse.gold"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/domains_check.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.epik.test/v2/domains/check",
        params: %{
          "DOMAINS" => "tribes.hodl"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/domains_check_400.json"),
       status_code: 400
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.epik.test/v2/domains/fedigold.xyz/create"
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/domains_create-fedigold.xyz.json"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        url: "https://api.epik.test/v2/availabletlds"
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/availabletlds.json"),
       status_code: 200
     }}
  end
end
