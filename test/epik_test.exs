defmodule EpikTest do
  use ExUnit.Case

  test "check_domains/1 returns a list of domains" do
    expected = %HTTPoison.Response{
      status_code: 200,
      body: %{
        "code" => 1000,
        "data" => %{
          "FEDIVERSE.GOLD" => %{
            "available" => 1,
            "domain" => "FEDIVERSE.GOLD",
            "premium" => 0,
            "price" => 5.49,
            "supported" => 1
          },
          "TRIBES.HOST" => %{
            "available" => 0,
            "available_reason" => "in use",
            "domain" => "TRIBES.HOST",
            "premium" => 0,
            "supported" => 1
          }
        },
        "message" => "Command completed successfully."
      }
    }

    assert {:ok, ^expected} = Epik.check_domains(["tribes.host", "fediverse.gold"])
  end

  test "check_domains/1 returns an error" do
    expected = %HTTPoison.Response{
      status_code: 400,
      body: %{
        "errors" => [
          %{
            "code" => 2,
            "description" => "Wrong param This tld (hodl) is not supported",
            "message" => "Invalid parameter"
          }
        ]
      }
    }

    assert {:ok, ^expected} = Epik.check_domains(["tribes.hodl"])
  end

  test "register_domain/2 with valid parameters" do
    expected = %HTTPoison.Response{
      status_code: 200,
      body: %{
        "code" => 1000,
        "data" => %{
          "FEDIGOLD.XYZ" => %{
            "error" => 0,
            "message" => "Successfully created",
            "payment" => %{
              "error" => 0,
              "paymentStatus" => true,
              "paymentValue" => 0.99,
              "period" => "1",
              "pricePerPeriod" => 0.99
            },
            "paymentStatus" => true,
            "paymentValue" => 0.99,
            "period" => "1",
            "pricePerPeriod" => 0.99
          }
        },
        "message" => "Command completed successfully.",
        "period" => "1",
        "total" => %{
          "amount" => 0.99,
          "message" => "Withdraw money successfully",
          "method" => "Balance",
          "success" => true
        }
      }
    }

    assert {:ok, ^expected} = Epik.register_domain("fedigold.xyz", 1)
  end

  test "list_tlds/0 returns a list of TLDs" do
    assert ["academy" | _] = Epik.list_tlds()
  end

  test "fetch_tlds/0 fetches a list of TLDs" do
    assert {:ok, ["academy" | _]} = Epik.fetch_tlds()
  end
end
