defmodule Epik do
  @moduledoc """
  Simple functions for interacting with the [Epik v2 API][1].

  [1]: https://docs.userapi.epik.com/v2/
  """

  @doc """
  Check the status of domain names, including their price and availability.

  ```elixir
  iex(1)> Epik.check_domains(["tribes.host", "fediverse.gold"])
  {:ok,
   %HTTPoison.Response{
     status_code: 200,
     body: %{
       "code" => 1000,
       "data" => %{
         "FEDIVERSE.GOLD" => %{
           "available" => 1,
           "domain" => "FEDIVERSE.GOLD",
           "premium" => 0,
           "price" => 5.49,
           "supported" => 1
         },
         "TRIBES.HOST" => %{
           "available" => 0,
           "available_reason" => "in use",
           "domain" => "TRIBES.HOST",
           "premium" => 0,
           "supported" => 1
         }
       },
       "message" => "Command completed successfully."
     }
   }}
  ```
  """
  @spec check_domains(domains :: [String.t()]) :: {:ok, HTTPoison.Response.t()} | {:error, any()}
  def check_domains(domains) when is_list(domains) do
    params = %{"DOMAINS" => Enum.join(domains, ",")}
    Epik.Client.get("/v2/domains/check", params)
  end

  @doc """
  Register a domain.

  ```elixir
  iex(1)> Epik.register_domain("fedigold.xyz", 1)
  {:ok,
   %HTTPoison.Response{
     status_code: 200,
     body: %{
       "code" => 1000,
       "data" => %{
         "FEDIGOLD.XYZ" => %{
           "error" => 0,
           "message" => "Successfully created",
           "payment" => %{
             "error" => 0,
             "paymentStatus" => true,
             "paymentValue" => 0.99,
             "period" => "1",
             "pricePerPeriod" => 0.99
           },
           "paymentStatus" => true,
           "paymentValue" => 0.99,
           "period" => "1",
           "pricePerPeriod" => 0.99
         }
       },
       "message" => "Command completed successfully.",
       "period" => "1",
       "total" => %{
         "amount" => 0.99,
         "message" => "Withdraw money successfully",
         "method" => "Balance",
         "success" => true
       }
     }
   }}
  ```
  """
  @spec register_domain(domain :: String.t(), years :: integer()) ::
          {:ok, HTTPoison.Response.t()} | {:error, any()}
  def register_domain(domain, years)
      when is_binary(domain) and is_integer(years) do
    domain = URI.encode(domain)
    body = %{"PERIOD" => years}
    Epik.Client.post("/v2/domains/#{domain}/create", body)
  end

  @doc """
  Returns a list of TLDs compatible with the Epik API.

  This is useful for client-side validation since some functions like
  `Epik.check_domains/1` will return an error if any one domain is invalid.

  This list is accurate at the time this package was published.
  To fetch the current list, use `Epik.fetch_tlds/0`.

  ```elixir
  iex(1)> Epik.list_tlds()
  ["academy", "accountant", "accountants", "actor", "ae.org", "ag", "agency",
   "airforce", "am", "apartments", "app", "archi", "army", "art", "associates",
   "at", "attorney", "auction", "audio", "baby", "band", "bar", "bargains",
   "bayern", "bazar", "bbs", "be", "beer", "best", "bet", "bible", "bid", "bike",
   "bingo", "bio", "bit", "biz", "black", "blackfriday", "blog", "blue", "boats",
   "bond", "boston", "boutique", "br.com", "builders", "business", "buzz", "ca",
   ...]
  ```
  """
  @spec list_tlds() :: [String.t()]
  defdelegate list_tlds(), to: Epik.Data

  @doc """
  Fetch a list of available TLDs from the API.

  ```elixir
  iex(1)> Epik.fetch_tlds()
  {:ok,
   ["academy", "accountant", "accountants", "actor", "ae.org", "ag", "agency",
    "airforce", "am", "apartments", "app", "archi", "army", "art", "associates",
    "at", "attorney", "auction", "audio", "baby", "band", "bar", "bargains",
    "bayern", "bazar", "bbs", "be", "beer", "best", "bet", "bible", "bid", "bike",
    "bingo", "bio", "bit", "biz", "black", "blackfriday", "blog", "blue", "boats",
    "bond", "boston", "boutique", "br.com", "builders", "business", ...]}
  ```
  """
  @spec fetch_tlds() :: {:ok, [String.t()]} | {:error, any()}
  def fetch_tlds() do
    with {:ok, %HTTPoison.Response{status_code: 200, body: %{"data" => data}}} <-
           Epik.Client.get("/v2/availabletlds") do
      domains = data |> Map.keys() |> Enum.sort()
      {:ok, domains}
    else
      error -> {:error, error}
    end
  end
end
